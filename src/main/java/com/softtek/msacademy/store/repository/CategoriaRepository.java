package com.softtek.msacademy.store.repository;

import com.softtek.msacademy.store.model.Categoria;
import org.springframework.data.repository.CrudRepository;

public interface CategoriaRepository extends CrudRepository<Categoria,Integer> {
}
