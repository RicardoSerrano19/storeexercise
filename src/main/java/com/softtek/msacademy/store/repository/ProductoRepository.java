package com.softtek.msacademy.store.repository;

import com.softtek.msacademy.store.model.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<Producto,Integer> {
}
