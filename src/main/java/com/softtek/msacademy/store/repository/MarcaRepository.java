package com.softtek.msacademy.store.repository;

import com.softtek.msacademy.store.model.Marca;
import org.springframework.data.repository.CrudRepository;

public interface MarcaRepository extends CrudRepository<Marca,Integer> {
}
