package com.softtek.msacademy.store.controller;

import com.softtek.msacademy.store.model.Producto;
import com.softtek.msacademy.store.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductoController {

    @Autowired
    ProductoRepository repository;

    @GetMapping(value = "/productos",produces = {"application/json"})
    public List<Producto> getAll(){
        return (List<Producto>) repository.findAll();
    }

    @GetMapping(value = "/productos/{id}")
    public Producto getById(@PathVariable Integer id) {
        return repository.findById(id).orElse(Producto.builder("No se encontro ningun Producto").build());
    }

    @PutMapping(value = "/productos")
    public Producto actualizarProducto(@RequestBody Producto producto){
        return repository.save(producto);
    }

    @PostMapping(value = "/productos")
    public Producto crearProducto(@RequestBody Producto producto){
        return repository.save(producto);
    }

    @DeleteMapping(value = "/productos/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        if(repository.findById(id).isPresent()){
            repository.deleteById(id);
            return new ResponseEntity<>(
                    "Producto eliminado",
                    HttpStatus.ACCEPTED
            );
        }else{
            return new ResponseEntity<>(
                    "El producto no se encuentra",
                    HttpStatus.NOT_FOUND
            );
        }
    }

}
