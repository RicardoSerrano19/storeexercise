package com.softtek.msacademy.store.controller;

import com.softtek.msacademy.store.model.Marca;
import com.softtek.msacademy.store.repository.MarcaRepository;
import com.softtek.msacademy.store.repository.MarcaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MarcaController {

    @Autowired
    MarcaRepository repository;

    @GetMapping(value = "/marcas",produces = {"application/json"})
    public List<Marca> getAll(){
        return (List<Marca>) repository.findAll();
    }

    @GetMapping(value = "/marcas/{id}")
    public Marca getById(@PathVariable Integer id) {
        return repository.findById(id).orElse(Marca.builder("No se encontro ninguna Marca").build());
    }

    @PutMapping(value = "/marcas")
    public Marca actualizarMarca(@RequestBody Marca Marca){
        return repository.save(Marca);
    }

    @PostMapping(value = "/marcas")
    public Marca crearMarca(@RequestBody Marca Marca){
        return repository.save(Marca);
    }

    @DeleteMapping(value = "/marcas/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        if(repository.findById(id).isPresent()){
            repository.deleteById(id);
            return new ResponseEntity<>(
                    "Marca eliminado",
                    HttpStatus.ACCEPTED
            );
        }else{
            return new ResponseEntity<>(
                    "El Marca no se encuentra",
                    HttpStatus.NOT_FOUND
            );
        }
    }

}
