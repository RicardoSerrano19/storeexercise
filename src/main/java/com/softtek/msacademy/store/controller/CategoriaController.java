package com.softtek.msacademy.store.controller;

import com.softtek.msacademy.store.model.Categoria;
import com.softtek.msacademy.store.repository.CategoriaRepository;
import com.softtek.msacademy.store.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoriaController {

    @Autowired
    CategoriaRepository repository;

    @GetMapping(value = "/categorias",produces = {"application/json"})
    public List<Categoria> getAll(){
        return (List<Categoria>) repository.findAll();
    }

    @GetMapping(value = "/categorias/{id}")
    public Categoria getById(@PathVariable Integer id) {
        return repository.findById(id).orElse(Categoria.builder("No se encontro ninguna Categoria").build());
    }

    @PutMapping(value = "/categorias")
    public Categoria actualizarCategoria(@RequestBody Categoria Categoria){
        return repository.save(Categoria);
    }

    @PostMapping(value = "/categorias")
    public Categoria crearCategoria(@RequestBody Categoria Categoria){
        return repository.save(Categoria);
    }

    @DeleteMapping(value = "/categorias/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        if(repository.findById(id).isPresent()){
            repository.deleteById(id);
            return new ResponseEntity<>(
                    "Categoria eliminado",
                    HttpStatus.ACCEPTED
            );
        }else{
            return new ResponseEntity<>(
                    "El Categoria no se encuentra",
                    HttpStatus.NOT_FOUND
            );
        }
    }

}
