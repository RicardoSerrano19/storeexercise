package com.softtek.msacademy.store.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;

@Data
@Builder(builderMethodName = "hiddenBuilder")
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idproducto;
    private String nombre;
    private String descripcion;
    @OneToOne
    @JoinColumn(name = "categoria", referencedColumnName = "idcategoria")
    private Categoria categoria;
    @OneToOne
    @JoinColumn(name = "marca", referencedColumnName = "idmarca")
    private Marca marca;
    private Integer cantidad;
    private Float precio;

    public static ProductoBuilder builder(String nombre) {
        return hiddenBuilder().nombre(nombre);
    }
}
