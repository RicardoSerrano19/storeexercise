package com.softtek.msacademy.store;

import com.softtek.msacademy.store.model.Categoria;
import com.softtek.msacademy.store.model.Marca;
import com.softtek.msacademy.store.model.Producto;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class StoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreApplication.class, args);
	}

}
