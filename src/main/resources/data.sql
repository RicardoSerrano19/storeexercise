DROP TABLE IF EXISTS  producto;
DROP TABLE IF EXISTS categoria;
DROP TABLE IF EXISTS marca;

CREATE TABLE IF NOT EXISTS categoria (
  idcategoria INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(45) NOT NULL,
  tipo VARCHAR(45) NOT NULL,
  PRIMARY KEY (idcategoria));

CREATE TABLE IF NOT EXISTS marca (
  idmarca INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(45) NOT NULL,
  descripcion VARCHAR(45) NOT NULL,
  PRIMARY KEY (idmarca));
CREATE TABLE IF NOT EXISTS producto (
    idproducto INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(45) NOT NULL,
    descripcion VARCHAR(45) NOT NULL,
    categoria INT ,
    marca INT ,
    cantidad INT  NOT NULL,
    precio FLOAT  NOT NULL,
    FOREIGN KEY (categoria) REFERENCES categoria(idcategoria) on delete SET NULL,
    FOREIGN KEY (marca) REFERENCES marca(idmarca) ON DELETE SET NULL
);

INSERT INTO marca (idmarca, nombre, descripcion) VALUES
(1, 'Huawei', 'Equipamiento de comunicaciones'),
(2, 'Cisco', 'Venta, mantenimiento y consultoria'),
(3, 'The Coca-Cola Company', 'Empresa productora de bebidas');

INSERT INTO categoria (idcategoria, nombre, tipo) VALUES
(1, 'Telecomunicaciones', 'Celulares'),
(2, 'Telecomunicaciones', 'Redes'),
(3, 'Alimentos', 'Bebidas');

INSERT INTO producto (idproducto, nombre, descripcion,categoria,marca,cantidad,precio ) VALUES
(1, 'Agua Ciel', 'Agua potable',3,3,85,12.5),
(2, 'Huawei Y9', 'Celular de gama media',1,1,12,5400),
(3, 'Fanta', 'Refresco de naranja',3,3,30,12.5),
(4, 'RV325', 'Router Dual Wan',2,2,3,179999.99);